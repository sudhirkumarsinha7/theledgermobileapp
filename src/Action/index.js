import {
    REGISTER,
    OTP,
    LOGIN,
    ALL_USER,
    USERINFO,
    USERUPDATE,
    HIDELODER,
    SHOWLODER,
    SHIPMENT,
    INVENTORY,
    INVENTORY_COUNTS,
    VACCINE_COUNTS,
    ADD_INVENTORY,
    UPDATE_SHIPMENT,
    ADD_SHIPMENT,
    PURCHASE_ORDER,
    PURCHASE_ORDER_IDS,
    ADD_PURCHASE_ORDER,
    ADD_PURCHASE_ORDER_IDS,
    SHIPMENT_COUNTS
} from '../constant';
import axios from 'axios';
import url from "../API";
import AsyncStorage from '@react-native-community/async-storage';
import { config } from "../config";
import setAuthToken from "../utils/setAuthToken";

export const saveUser = (userdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: REGISTER,
        payload: userdata
    })
    try {
        const response = await axios.post(config().registerUrl, userdata);
        //const response = await axios.post(`${url}/usermanagement/api/auth/register`, userdata);
        console.log("Action_saveUser_response", response);
        if (response.data.message === "Registration Success.") {
            callback()
        }
        return response;
    }
    catch (e) {
        console.log("Action_saveUser_error", e.response);
        const err = e.response.data.data[0];
        if (err.msg === "E-mail already in use") {
            return e.response;
        } else {
            return e
        }

    }
    finally {
        loadingOff(dispatch)
    }
}

export const verify_otp = (otpdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: OTP,
        payload: otpdata
    })
    try {
        const response = await axios.post(config().verifyOtpUrl, otpdata);
        //const response = await axios.post(`${url}/usermanagement/api/auth/verify-otp`, otpdata);
        console.log("Action_verify_otp_response", response);
        if (response.data.message === "Account confirmed success.") {
            callback()
            // alert('Successfully Verify')
        }
        return response;
    }
    catch (e) {
        console.log("Action_verify_otp_error", e.response);
        //alert(e);
        return e.response
    }
    finally {
        loadingOff(dispatch)
    }
}

export const resend_otp = (otpdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    // dispatch({
    //     type: OTP,
    //     payload: otpdata
    // })
    try {
        const response = await axios.post(`${url}/usermanagement/api/auth/resend-verify-otp`, otpdata);
        console.log("Action_resend_otp_response", response);
        if (response.data.message === "Confirm otp sent.") {
            alert('Resend OTP Sent Successfully')
        }
        return response;
    }
    catch (e) {
        console.log("Action_resend_otp_error", e.response);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const login = (logindata, rem, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: LOGIN,
        payload: rem
    })

    try {
        console.log(config().loginUrl, logindata,getState().login.rem);

        const response = await axios.post(config().loginUrl, logindata);
        
        console.log("Action_login_response", response);
        if (response.data.message === "Login Success.") {
            callback()
            if (getState().login.rem) {
                console.log('true', getState().login.rem);
                await AsyncStorage.setItem('logged', '1');
                await AsyncStorage.setItem('token', response.data.data.token);
                setAuthToken(response.data.data.token);


                dispatch({
                    type: LOGIN,
                    payload: response.data.data.token
                })
            } else {
                console.log('false', getState().login.rem);
                setAuthToken(response.data.data.token);

                dispatch({
                    type: LOGIN,
                    payload: response.data.data.token
                })
            }
        }
        return response;
    }
    catch (e) {
        console.log("Action_login_error", e)
        console.log("Action_login_error", e.response)
        const err = e.response.data.message;
        if (err === "Email or Password wrong.") {
            return e.response;
        } else {
            return e
        }

    }
    finally {
        loadingOff(dispatch)
    }
}

export const upload_Image = (image, imageredux, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        const configs = {
            headers: {
                'content-type': 'multipart/form-data',
            },
        };
        response = await axios.post(config().upload, image);
        //response = await axios.post(`${url}/usermanagement/api/auth/upload`, image, configs);
        if (response.data.message === "Updated") {
            alert('Image successfully updated')
            dispatch({
                type: USERUPDATE,
                payload: { profile_picture: imageredux }
            })
        }
        console.log("Action_upload_Image_response", response);
        return response;
    } catch (e) {
        console.log("Action_upload_Image_error", e.response);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const user_update = (data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        console.log("profileupdate", config().updateProfileUrl, data);

        response = await axios.post(config().updateProfileUrl, data);
        //response = await axios.post(`${url}/usermanagement/api/auth/updateProfile`, data);
        if (response.status === 200) {
            // alert('Successfully updated')
            dispatch({
                type: USERUPDATE,
                payload: data
            })
        }
        console.log("Action_user_update_response", response);
        return response;
    } catch (e) {
        console.log("Action_user_update_error", e.response);
        console.log("Action_user_update_error", e);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const userinfo = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().userInfoUrl);
        // const response = await axios.get(`${url}/usermanagement/api/auth/userInfo`);
        console.log("Action_userinfo_response", response.data);
        dispatch({
            type: USERINFO,
            payload: response.data.data
        })
        return response.data;
    }
    catch (e) {
        console.log("Action_userinfo_error", e.response)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const inventory = (skip, limit) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(`${config().inventoriesUrl}?skip=${skip}&limit=${limit}`);
        // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_inventory_response", response);
        if (response.status === 200)
            dispatch({
                type: INVENTORY,
                payload: response.data.data
            })
        dispatch({
            type: INVENTORY_COUNTS,
            payload: response.data.counts
        })
        dispatch({
            type: VACCINE_COUNTS,
            payload: response.data.dict
        })

        return response;
    }
    catch (e) {
        console.log("Action_inventory_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const add_inventory = (data, callback = () => { }) => async (dispatch, getState) => {
    try {
        console.log('start', { data: data }, config().addInventoryUrl);
        const responses = await Promise.all(data.map((item) => {
            return axios.post(config().addInventoryUrl, { data: item });
            //return axios.post(`${url}/inventorymanagement/api/inventory/addNewInventory`, { data: item })
        }))
        //const response = await axios.post(`${url}/inventorymanagement/api/inventory/addNewInventory`,data);
        console.log("Action_add_inventory _response", responses);
        //error alert           
        for (let index in responses) {
            let response = responses[index]
            if (response.status !== 200) {
                //if one status is failed so it is going to the catch
                //alert('Failed to Add Inventory')
                throw new Error('inventory unsuccessful')
            }
        }
        //alert('Successfully Add Inventory')

        dispatch({
            type: ADD_INVENTORY,
            payload: data
        })
        callback()
        return responses;
    }
    catch (e) {
        console.log("Action_add_inventory_error", e.response)
        return e
        // alert(e);
    }
}

export const fetch_shipmemnt = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory");
    try {
        const response = await axios.get(config().shipmentsSearch + 'SHP2345'); //SHP9876
        // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_fetch_shipmemnt _response", response.data);
        if (response.data.data)
            dispatch({
                type: SHIPMENT,
                payload: response.data
            })
        return response.data;
    }
    catch (e) {
        console.log("Action_fetch_shipmemnt_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const create_shipmemnt = (data, address, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory", data);
    try {
        console.log(config().createShipmentUrladdress + address);

        const response = await axios.post(config().createShipmentUrladdress + address, { data: data });
        console.log("Action_create_shipmemnt_response", response);
        if (response.status === 200) {
            dispatch({
                type: UPDATE_SHIPMENT,
                payload: data
            })
        }
        return response;
    }
    catch (e) {
        console.log("Action_create_shipmemnt_error", e.response)
        console.log("Action_create_shipmemnt_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const create_new_shipmemnt = (data, address, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("create_new_shipmemnt", { data });

    try {
        const response = await axios.post(config().createShipmentUrl, { data });
        console.log("Action_create_new_shipmemnt_response", response);
        if (response.status === 200) {
            dispatch({
                type: ADD_SHIPMENT,
                payload: data
            })
        }
        return response;
    }
    catch (e) {
        console.log("Action_create_new_shipmemnt_error", e.response)
        console.log("Action_create_new_shipmemnt_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fetchPublisherShipments = (skip, limit) => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("fetchPublisherShipments");
    try {
        const response = await axios.get(`${config().shipmentsUrl}?skip=${skip}&limit=${limit}`); //SHP9876
        // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_fetchPublisherShipments_response", response.data);
        if (response.status === 200)
            dispatch({
                type: SHIPMENT,
                payload: response.data.data
            })
        dispatch({
            type: SHIPMENT_COUNTS,
            payload: response.data.counts
        })
        return response;
    }
    catch (e) {
        console.log("Action_fetchPublisherShipments_error", e.response)
        console.log("Action_fetchPublisherShipments_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fetchAllUsers = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().getAllUsersUrl);
        console.log("Action_fetchAllUsers_response", response);
        if (response.status === 200)
            dispatch({
                type: ALL_USER,
                payload: response.data.data
            })
        return response.data.data;
    }
    catch (e) {
        console.log("Action_fetchAllUsers_error", e.response)
        console.log("Action_fetchAllUsers_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const Create_Purchase_Order = (data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        console.log("Create_Purchase_Order start", data);

        const response = await axios.post(config().createPurchaseOrderUrl, { data: data });
        console.log("Action_Create_Purchase_Order_response", response);
        if (response.status === 200) {
            dispatch({
                type: ADD_PURCHASE_ORDER,
                payload: data
            })
            dispatch({
                type: ADD_PURCHASE_ORDER_IDS,
                payload: response.data.orderID
            })
            callback()
        }

        return response;
    }
    catch (e) {
        console.log("Action_Create_Purchase_Order_error", e.response)
        console.log("Action_Create_Purchase_Order_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fetch_Purchase_Orders_ids = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().fetchAllPurchaseOrdersUrl);
        console.log("Action_fetch_Purchase_Orders_ids_response", response);
        if (response.status === 200) {
            dispatch({
                type: PURCHASE_ORDER_IDS,
                payload: response.data
            })
        }
        return response;
    }
    catch (e) {
        console.log("Action_fetch_Purchase_Orders_ids_error", e.response)
        console.log("Action_fetch_Purchase_Orders_ids_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fetch_Purchase_Orders = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().fetchPurchaseOrderStatisticsUrl);
        console.log("Action_fetch_Purchase_Orders_response", response);
        if (response.status === 200) {
            dispatch({
                type: PURCHASE_ORDER,
                payload: response.data
            })
        }
        return response;
    }
    catch (e) {
        console.log("Action_fetch_Purchase_Orders_error", e.response)
        console.log("Action_fetch_Purchase_Orders_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const selected_po = (poid, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().fetchAllPurchaseOrderUrl + poid);
        console.log("Action_selected_po_response", response);
        return response.data.data;
    }
    catch (e) {
        console.log("Action_selected_po_error", e.response)
        console.log("Action_selected_po_error", e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const addInventory = (body, body2, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        console.log("body", body, body2);
        const response = await axios.post(config().addInventory, { data: body });
        console.log("Action_addInventory_response", response);
        if (response.status === 200) {
            console.log("hello");
            try {
                const response = await axios.post(config().createShipmentUrl, { data: body2 });
                console.log("Action_create_new_shipmemnt_response", response);
                if (response.status === 200) {
                    dispatch({
                        type: ADD_SHIPMENT,
                        payload: body2
                    })
                }
                return response;
            }
            catch (e) {
                console.log("Action_create_new_shipmemnt_error", e.response)
                console.log("Action_create_new_shipmemnt_error", e)
                return e;
            }
        }

        callback()
        return response;
    }
    catch (e) {
        const response = await axios.post(config().updateInventory, { data: body });
        console.log("response",response);
        if (response.status === 200) {
            console.log("hello catch");
            try {
                const response = await axios.post(config().createShipmentUrl, { data: body2 });
                console.log("Action_create_new_shipmemnt_response", response);
                if (response.status === 200) {
                    dispatch({
                        type: ADD_SHIPMENT,
                        payload: body2
                    })
                }
                return response;
            }
            catch (e) {
                console.log("Action_create_new_shipmemnt_error", e.response)
                console.log("Action_create_new_shipmemnt_error", e)
                return e;
            }
        }
        // console.log("Action_addInventory_error", e.response)
        // console.log("Action_saddInventory_error", e)
        callback()
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const loadingOn = (dispatch) => {
    dispatch({
        type: SHOWLODER,
    });
};

export const loadingOff = (dispatch) => {
    dispatch({
        type: HIDELODER,
    });
};