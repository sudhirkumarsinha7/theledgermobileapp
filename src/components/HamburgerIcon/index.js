import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { scale, moderateScale, verticalScale} from '../Scale';
import { TouchableOpacity, View, Text, Image, Dimensions, TouchableHighlight } from 'react-native';

class HamburgerIcon extends Component {
    render() {
        return (
            <TouchableOpacity
                style={{
                    width: scale(25),
                    height: scale(25),
                    
                    justifyContent: "center",
                    alignItems:"center",
                }}
                onPress={() => {
                    this.props.navigation.toggleDrawer();
                }}>
                <Image
                    style={{ width: scale(22),
                        height: scale(22), borderWidth: 0, }}
                    source={require("../../assets/Menu.png")}
                    resizeMode='contain'
                />
            </TouchableOpacity>
        )
    };
}
export default withNavigation(HamburgerIcon);