import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    BackHandler,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from "../HamburgerIcon"

export default class Header extends Component {
    constructor() {
        super();
        this.state = {

        };
    }

    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 0, y: 1 }}
                colors={['#0093E9', '#36C2CF']}
                style={{ width: Dimensions.get('window').width, height: scale(180), borderBottomLeftRadius: 24, borderBottomRightRadius: 24 }}>
                <View style={{ marginTop: verticalScale(50), marginLeft: scale(16), flexDirection: "row", alignItems: "center", }}>
                    <HamburgerIcon />
                    <Text style={{ marginLeft: scale(23), fontSize: scale(23), fontFamily: "Roboto-Bold", color: "#FFFFFF", width: "65%", fontWeight: "bold" }}>{this.props.name}</Text>
                    {this.props.scan ?
                        <TouchableOpacity onPress={this.props.onPress}>
                            <Image
                                style={{ width: scale(13.5), height: scale(15), alignSelf: "center", }}
                                source={require("../../assets/blackberry.png")}
                                resizeMode='contain'
                            />
                        </TouchableOpacity> : null}
                    <View style={{ justifyContent: "flex-end",right:16,position:"absolute"}}>
                        <Image
                            style={{ width: scale(13.5), height: scale(15), }}
                            source={require("../../assets/Bell.png")}
                            resizeMode='contain'
                        />
                    </View>
                </View>
            </LinearGradient>
        );
    }
}