import React, { Component } from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    BackHandler,
    View,
    Image,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Header from "../Header"

export default class Skeleton extends Component {
    constructor() {
        super();
        this.state = {

        };
    }

    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Shipment'} />
                
                    <ScrollView style={{ marginTop: verticalScale(-75) }}>
                        <SkeletonPlaceholder>
                            <SkeletonPlaceholder.Item marginLeft={scale(10)} marginRight={scale(10)} >
                                <SkeletonPlaceholder.Item flexDirection="row" justifyContent={"space-between"} marginLeft={scale(10)} marginRight={scale(10)}>
                                    <SkeletonPlaceholder.Item flexDirection="row" backgroundColor={"#FFFFFF"} height={scale(92)} width={scale(150)} borderRadius={8}>
                                        <SkeletonPlaceholder.Item>
                                            <SkeletonPlaceholder.Item width={scale(40)} height={scale(40)} borderRadius={50} marginLeft={scale(15)} marginTop={verticalScale(15)} />
                                            <SkeletonPlaceholder.Item width={80} height={20} borderRadius={4} marginTop={verticalScale(10)} marginLeft={scale(15)} />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item marginLeft={10} marginTop={verticalScale(35)} >
                                            <SkeletonPlaceholder.Item width={60} height={20} borderRadius={4} />

                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>

                                    <SkeletonPlaceholder.Item flexDirection="row" backgroundColor={"#FFFFFF"} height={scale(92)} width={scale(150)} borderRadius={8} marginLeft={scale(10)}>
                                        <SkeletonPlaceholder.Item>
                                            <SkeletonPlaceholder.Item width={scale(40)} height={scale(40)} borderRadius={50} marginLeft={scale(15)} marginTop={verticalScale(15)} />
                                            <SkeletonPlaceholder.Item width={80} height={20} borderRadius={4} marginTop={verticalScale(10)} marginLeft={scale(15)} />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item marginLeft={10} marginTop={verticalScale(35)} >
                                            <SkeletonPlaceholder.Item width={60} height={20} borderRadius={4} />

                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder.Item>

                                <SkeletonPlaceholder.Item flexDirection="row" justifyContent={"space-between"} marginLeft={scale(10)} marginRight={scale(10)} marginTop={verticalScale(20)}>
                                    <SkeletonPlaceholder.Item flexDirection="row" backgroundColor={"#FFFFFF"} height={scale(92)} width={scale(150)} borderRadius={8}>
                                        <SkeletonPlaceholder.Item>
                                            <SkeletonPlaceholder.Item width={scale(40)} height={scale(40)} borderRadius={50} marginLeft={scale(15)} marginTop={verticalScale(15)} />
                                            <SkeletonPlaceholder.Item width={80} height={20} borderRadius={4} marginTop={verticalScale(10)} marginLeft={scale(15)} />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item marginLeft={10} marginTop={verticalScale(35)} >
                                            <SkeletonPlaceholder.Item width={60} height={20} borderRadius={4} />

                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>

                                    <SkeletonPlaceholder.Item flexDirection="row" backgroundColor={"#FFFFFF"} height={scale(92)} width={scale(150)} borderRadius={8} marginLeft={scale(10)}>
                                        <SkeletonPlaceholder.Item>
                                            <SkeletonPlaceholder.Item width={scale(40)} height={scale(40)} borderRadius={50} marginLeft={scale(15)} marginTop={verticalScale(15)} />
                                            <SkeletonPlaceholder.Item width={80} height={20} borderRadius={4} marginTop={verticalScale(10)} marginLeft={scale(15)} />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item marginLeft={10} marginTop={verticalScale(35)} >
                                            <SkeletonPlaceholder.Item width={60} height={20} borderRadius={4} />

                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder.Item>

                                <SkeletonPlaceholder.Item width={scale(159)} height={scale(40)} borderRadius={10} marginLeft={scale(10)} marginTop={verticalScale(20)} />

                                <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(20)} width={scale(328)} backgroundColor={"#FFFFFF"} borderRadius={8} height={scale(135)} >
                                    <SkeletonPlaceholder.Item width={scale(30)} marginTop={verticalScale(10)} marginLeft={scale(10)} borderRadius={10} height={scale(30)} />
                                    <SkeletonPlaceholder.Item marginLeft={scale(20)} marginTop={verticalScale(10)}>
                                        <SkeletonPlaceholder.Item width={scale(130)} height={scale(20)} borderRadius={4} />
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>
                                    <SkeletonPlaceholder.Item flexDirection={"column"} marginLeft={scale(20)} marginTop={verticalScale(10)} >
                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={4} />

                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={15} marginTop={verticalScale(30)} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder.Item>

                                <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(20)} width={scale(328)} backgroundColor={"#FFFFFF"} borderRadius={8} height={scale(135)} >
                                    <SkeletonPlaceholder.Item width={scale(30)} marginTop={verticalScale(10)} marginLeft={scale(10)} borderRadius={10} height={scale(30)} />
                                    <SkeletonPlaceholder.Item marginLeft={scale(20)} marginTop={verticalScale(10)}>
                                        <SkeletonPlaceholder.Item width={scale(130)} height={scale(20)} borderRadius={4} />
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>
                                    <SkeletonPlaceholder.Item flexDirection={"column"} marginLeft={scale(20)} marginTop={verticalScale(10)} >
                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={4} />

                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={15} marginTop={verticalScale(30)} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder.Item>

                                <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(20)} width={scale(328)} backgroundColor={"#FFFFFF"} borderRadius={8} height={scale(135)} >
                                    <SkeletonPlaceholder.Item width={scale(30)} marginTop={verticalScale(10)} marginLeft={scale(10)} borderRadius={10} height={scale(30)} />
                                    <SkeletonPlaceholder.Item marginLeft={scale(20)} marginTop={verticalScale(10)}>
                                        <SkeletonPlaceholder.Item width={scale(130)} height={scale(20)} borderRadius={4} />
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                        <SkeletonPlaceholder.Item flexDirection={"row"} marginTop={verticalScale(10)}>
                                            <SkeletonPlaceholder.Item
                                                //marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                            />
                                            <SkeletonPlaceholder.Item
                                                // marginTop={6}
                                                width={scale(80)}
                                                height={scale(20)}
                                                borderRadius={4}
                                                marginLeft={scale(20)}
                                            />
                                        </SkeletonPlaceholder.Item>
                                    </SkeletonPlaceholder.Item>
                                    <SkeletonPlaceholder.Item flexDirection={"column"} marginLeft={scale(20)} marginTop={verticalScale(10)} >
                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={4} />

                                        <SkeletonPlaceholder.Item width={scale(60)} height={scale(20)} borderRadius={15} marginTop={verticalScale(30)} />
                                    </SkeletonPlaceholder.Item>
                                </SkeletonPlaceholder.Item>

                            </SkeletonPlaceholder.Item>

                        </SkeletonPlaceholder>
                    </ScrollView>
                
            </View>
        );
    }
}