import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    StatusBar,
    Switch,
    Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import Header from "../../components/Header"
import PopUp from "../../components/PopUp";
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, create_shipmemnt,addInventory } from '../../Action'

class Scan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //variable to hold the qr value
            qrvalue: '',
            opneScanner: true,
            id: '',
            isModalVisible: false,
            isModalVisible2: false
        }
    }
    send_Shipment = async () => {
        // In Transit
        console.log('id', this.state.id);

        const data = this.props.shipment;
        for (var i = 0; i < data.length; i++) {
            if (data[i].shipmentId === this.state.id) {
                if (data[i].status === "In Transit") {

                    //const finaldata = { ...data, status: "In Transit" }
                    const finaldata = { ...data[i], status: "Received" }
                    console.log('ss', { ...data[i], status: "Received" }, finaldata);

                    const result = await this.props.create_shipmemnt(finaldata, data[i].receiver);
                    if (result.status === 200) {
                        this.setState({
                            isModalVisible: true
                        })
                        // alert("Received Successfully")
                        //this.setState({ opneScanner: true });
                        //this.props.navigation.navigate('Shipment_Orders')
                    } else {
                        this.setState({
                            isModalVisible2: true
                        })
                    }
                    console.log('rrrr', result);
                }
                else {
                    this.setState({
                        isModalVisible2: true
                    })
                }
            }
        }

    }

    ok = () => {
        this.setState({ isModalVisible: false, opneScanner: true })
        //Inventory
        this.props.navigation.navigate('Inventory');
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }
    AsyncAlert(message) {
        return new Promise((resolve, reject) => {
            Alert.alert(
                '',
                message,
                [
                    { text: 'OK', onPress: () => resolve('YES') },
                ],
                { cancelable: false }
            )
        })
    }
    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    onBarcodeScan(qrvalue) {
        //called after te successful scanning of QRCode/Barcode
        this.setState({ opneScanner: false })
        console.log('qr', qrvalue);
        if (!this.isJson(qrvalue)) {
            this.AsyncAlert("invalid QR Code").then(() => {
                this.setState({ opneScanner: true })
            })
            return
        }

        console.log('json', JSON.parse(qrvalue));
        const value = JSON.parse(qrvalue);
        console.log('ggg', value);
        this.setState({ qrvalue: value.shippingId });
        this.setState({ opneScanner: false });
        this.setState({
            id: value.shippingId,
        })
    }
    onOpneScanner() {
        var that = this;
        //To Start Scanning
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'CameraExample App Camera Permission',
                        'message': 'CameraExample App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //If CAMERA Permission is granted
                        that.setState({ qrvalue: '' });
                        that.setState({ opneScanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            //Calling the camera permission function
            requestCameraPermission();
        } else {
            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
        }
    }
    render() {
        console.log('scan order', this.props.shipment);
        if (!this.state.opneScanner) {
            return (
                <View style={{
                    flex: 1,
                    alignItems: "center"
                }}>
                    <StatusBar backgroundColor="#0093E9" />
                    <Header name={'Receive Shipment'} scan={true} onPress={() => this.onOpneScanner()} />
                    <PopUp isModalVisible={this.state.isModalVisible}
                        image={require('../../assets/Sucess.png')}
                        text="Success!"
                        text2="Your Shipment has been received successfully !"
                        label="Ok"
                        Ok={() => this.ok()} />
                    <PopUp isModalVisible={this.state.isModalVisible2}
                        image={require('../../assets/fail.png')}
                        text="Fail!"
                        text2="Something went wrong. Please try again."
                        label="Try again"
                        Ok={() => this.fail()} />

                    {this.props?.shipment?.map((item, index) => (
                        <>
                            {item?.shipmentId === this.state?.id ?
                                <View style={{ marginTop: verticalScale(-71), width: scale(328), height: scale(117), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                                    <View style={{ flexDirection: "row", }}>

                                        <View style={{ width: scale(30), marginTop: verticalScale(10), marginLeft: scale(10), borderRadius: 10, height: scale(30) }}>
                                            <Image style={{ width: scale(30), height: scale(30) }}
                                                resizeMode='center'
                                                source={require('../../assets/user.png')} />
                                        </View>

                                        <View style={{ width: "50%", marginTop: verticalScale(10), marginLeft: scale(10), flexDirection: "column" }}>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>{item?.deliveryTo}</Text>

                                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Shipment ID</Text>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000" }}>{item?.shipmentId}</Text>
                                            </View>

                                            <View style={{ marginTop: verticalScale(10), flexDirection: "row", }}>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Quantity</Text>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{item?.products[0]?.quantity}</Text>
                                            </View>

                                            <View style={{ marginTop: verticalScale(10), flexDirection: "row", }}>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Client</Text>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{item?.client}</Text>
                                            </View>

                                        </View>

                                        <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), marginRight: scale(10), flexDirection: "column" }}>
                                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{item?.estimateDeliveryDate}</Text>

                                        </View>

                                    </View>
                                </View>
                                : null
                            }
                        </>
                    ))}

                    {this.props?.shipment?.map((item, index) => (
                        <>
                            {item?.shipmentId === this.state?.id ?
                                item?.products?.map((item2, index2) => (
                                    <View style={{ marginTop: verticalScale(15), width: scale(328), height: scale(210), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                                        <View style={{ flexDirection: "row", }}>

                                            <View style={{ marginLeft: scale(10), marginTop: verticalScale(10), width: scale(25), height: scale(25), borderRadius: 400, backgroundColor: "#C1E3F2", justifyContent: "center", alignItems: "center" }}>
                                                <Text style={{ fontSize: scale(8), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{item2?.productName}</Text>
                                            </View>

                                            <View style={{ marginLeft: scale(8), marginTop: verticalScale(15), width: "60%", }}>
                                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", }}> {item2?.productName}</Text>
                                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(10) }} numberOfLines={1}></Text>
                                                </View>

                                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Manufacturer</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item2?.manufacturerName}</Text>
                                                </View>

                                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Qty</Text>
                                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item2?.quantity}</Text>
                                                </View>


                                                <View>
                                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Mfg Date</Text>
                                                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item2?.manufacturingDate}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Exp Date</Text>
                                                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item2?.expiryDate}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Batch No</Text>
                                                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item2?.batchNumber}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Serial No</Text>
                                                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }} numberOfLines={1}>{item2?.serialNumber}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                )) : null
                            }
                        </>
                    ))}

                    <View style={{ flexDirection:"row",width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(90), position: 'absolute', bottom: 10, justifyContent: "space-between" }}>

                        <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center",borderColor:"#0093E9",borderWidth:1 }}
                            disabled={true}>
                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../assets/edit.png')} />
                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold",marginLeft:scale(10) }}>EDIT</Text>
                        </TouchableOpacity>


                        <TouchableOpacity onPress={() => { this.send_Shipment() }}
                            style={{ width: scale(195), height: scale(40), borderRadius: 8,flexDirection:"row",backgroundColor:"#FA7923",alignItems:"center",justifyContent:"space-evenly" }}>
                            <Image style={{ width: scale(15), height: scale(15), }}
                            resizeMode='center'
                            source={require('../../assets/Receiveshipment.png')} />
                                <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>RECEIVE SHIPMENT</Text>
                            
                        </TouchableOpacity>
                    </View>
                    {/* {!this.state.hide ?
                                <TouchableOpacity onPress={() => this.onOpneScanner()}
                                    style={{ marginLeft: scale(-10), marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                                    <Image style={{ width: scale(15), height: scale(15) }}
                                        resizeMode='center'
                                        source={require('../../../assets/Scan1.png')} />
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>SCAN</Text>
                                </TouchableOpacity> : null}
                        </View>
                    </View>

                    {this.state.qrvalue === this.props.shipment[id].shipmentId ?
                        <TouchableOpacity onPress={() => { this.send_Shipment() }}
                            style={{ width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(90), position: 'absolute', bottom: 0, }}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#0093E9', '#36C2CF']}
                                style={{ width: scale(328), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Send Shipment</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        : null} */}
                </View>
            )
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <CameraKitCameraScreen
                        showFrame={true}
                        //Show/hide scan frame
                        scanBarcode={true}
                        //Can restrict for the QR Code only
                        laserColor={'green'}
                        //Color can be of your choice
                        frameColor={'green'}
                        //If frame is visible then frame color
                        colorForScannerFrame={'black'}
                        //Scanner Frame color
                        onReadCode={event =>
                            this.onBarcodeScan(event.nativeEvent.codeStringValue)
                        }
                    />
                </View>
            )
        }
    }
}

const customiseShipmentData = (shipment) => {

    let data = [];
    for (var i = 0; i < shipment.length; i++) {
        const Object = {};
        if (shipment[i].status === "In Transit") {
            Object = shipment[i]
        }
        data.push(Object);
    }

    console.log('data', data);

    return data;

}

function mapStateToProps(state) {
    return {
        shipment: customiseShipmentData(state.shipment.shipmentdata),
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, create_shipmemnt,addInventory })(Scan)