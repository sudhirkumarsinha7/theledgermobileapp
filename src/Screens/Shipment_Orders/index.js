import React from 'react';
import {
    Button,
    View,
    Text,
    StatusBar,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    ActivityIndicator,
    KeyboardAvoidingView,
    RefreshControl,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders} from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header from "../../components/Header"
import Empty_Card from "../../components/Empty_Card";
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import HamburgerIcon from '../../components/HamburgerIcon';
import Shipment_Order_Card from "../../components/Shipment_Order_Card";


class Shipment_Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment');
        return true;
    }

    _click = (item, index) => {
        console.log('click', item);

        this.props.navigation.navigate('Shipment_Order_ShowMore', { data: item.shipmentId, id: index });
    }
    GetData = async () => {
        await this.props.userinfo()
        await this.props.inventory(0,5)
        await this.props.fetchPublisherShipments(0,5)
        await this.props.fetchAllUsers()
        await this.props.fetch_Purchase_Orders_ids()
        await this.props.fetch_Purchase_Orders()
    }
    onRefresh = async () => {
        //Call the Service to get the latest data
        this.GetData();
      }
    render() {
        console.log('shipmeny order', this.props.shipment);
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View style={{
                flex: 1,
                alignItems: "center"
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Shipment Orders'} />

                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} 
                    refreshControl={
                        <RefreshControl
                          //refresh control used for the Pull to Refresh
                          onRefresh={this.onRefresh.bind(this)}
                          colors={['#a076e8', '#5dc4dd']}
                        />
                      }
                      style={{ marginTop: verticalScale(-90) }}>


                    {this.props.shipment.length === 0 ?
                        // <View style={{ marginTop: verticalScale(50), backgroundColor: "#FFFFFF", borderRadius: 8, width: scale(328), alignItems: "center",justifyContent:"center",height:scale(421) }}>
                            
                        //     <Text style={{ fontSize: scale(20),width:scale(168),textAlign:"center" }}numberOfLines={2}>Looks like your Orders are Empty!</Text>
                        //     <Image style={{ width: scale(260), height: scale(211), marginTop: verticalScale(11) }}
                        //         resizeMode='cover'
                        //         source={require('../../assets/Empty.png')} />
                            
                        // </View> 
                        <Empty_Card Text="Orders are Empty!"/>
                        :
                        this.props.shipment.map((item, index) => (
                            <>
                                {
                                    item.status === "Shipped" ?
                                        <Shipment_Order_Card button={item} onClick={() => { this._click(item, index) }} />
                                        : null}
                            </>
                        ))
                    }
                    <View style={{ height: scale(20) }} />

                </ScrollView>

            </View>
        )
    }
}

const customiseShipmentData = (shipment) => {

    let data = [];
    for (let i in shipment) {
        const Object = {};
        const total = 0;
        if (shipment[i].status === "Shipped") {
            Object = shipment[i]
            Object['isExpanded'] = false
            data.push(Object);
        }
    }

    console.log('data', data);

    return data.reverse();

}

function mapStateToProps(state) {
    return {
        shipment: customiseShipmentData(state.shipment.shipmentdata),
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders})(Shipment_Orders)