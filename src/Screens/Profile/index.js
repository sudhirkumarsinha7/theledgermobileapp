import React from 'react';
import {
    Button,
    View,
    Text,
    StatusBar,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    ActivityIndicator,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, upload_Image, user_update } from '../../Action';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header from "../../components/Header"
import ImagePicker from 'react-native-image-picker';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import HamburgerIcon from '../../components/HamburgerIcon';
import PopUp from "../../components/PopUp";

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            image: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg",
            filePath: {},
            isModalVisible: false,
            isModalVisible2: false
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment');
        return true;
    }

    componentDidMount = async () => {
        this._data();
        // const token = await AsyncStorage.getItem('token');
        // console.log('protoken', token);

        // console.log('renderprofile', this.props.user);
        // const { user } = this.props;
        // //let {role,organisation,email,phone} = this.state;
        // this.setState({
        //     profile_picture: user.profile_picture,
        //     name: user.name,
        //     role: user.role,
        //     organisation: user.organisation,
        //     affiliateOrganisation: user.affiliateOrganisation,
        //     address: user.address,
        //     email: user.email,
        //     phone: user.phone,
        //     status: user.status,
        // })
    }

    _data = async () => {
        const token = await AsyncStorage.getItem('token');
        console.log('protoken', token);

        console.log('renderprofile', this.props.user);
        const { user } = this.props;
        //let {role,organisation,email,phone} = this.state;
        this.setState({
            profile_picture: user.profile_picture,
            name: user.name,
            role: user.role,
            organisation: user.organisation,
            affiliateOrganisation: user.affiliateOrganisation,
            address: user.address,
            email: user.email,
            phone: user.phone,
            status: user.status,
        })
    }

    _edit() {
        this.setState({ edit: true })
    }
    _cancel() {
        this._data();
        this.setState({ edit: false })

    }

    _save = async () => {
        var {
            name,
            organisation,
            affiliateOrganisation,
            phone
        } = this.state

        const data = {
            "name": name,
            "organisation": organisation,
            "affiliateOrganisation": affiliateOrganisation,
            "phone": phone
        }
        //^[^-\s][a-zA-Z0-9_\s-]+$
        const re = /^(?!\s*$|\s).*$/;

        //if (name === "" || organisation === "" || affiliateOrganisation === "" || phone === "") {
        if (!re.test(name) || !re.test(organisation) || !re.test(affiliateOrganisation) || !re.test(phone)) {
            // const result = await this.props.user_update(data)
            // if (result.status === 200) { this.setState({ edit: false })}
            alert('All Fields Are Mandatory Or Input Should not start with blank')

        } else {
            const result = await this.props.user_update(data)
            if (result.status === 200) { this.setState({ edit: false, isModalVisible: true }) }
            else {
                this.setState({ edit: false, isModalVisible2: true })
            }


            //this.props.user_update(data)
            // const result = await this.props.user_update(data)
            // if (result.status === 200) { this.setState({ edit: false })}
        }

    }

    chooseFile = () => {
        var options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else if (response.uri) {
                this.setState({ filePath: response }, () => this.handleUploadPhoto());
            }
        });
    };
    createFormData = (filePath, body) => {
        const data = new FormData();

        data.append("profile", {
            name: filePath.fileName,
            type: filePath.type,
            uri:
                Platform.OS === "android" ? filePath.uri : filePath.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        console.log('image data', data);
        return data;
    };
    handleUploadPhoto = async () => {
        const result = await this.props.upload_Image(this.createFormData(this.state.filePath, { userId: "123" }), this.state.filePath.uri)
    };

    render() {
        console.log('reprofile', this.props.user);
        const { user } = this.props;
        console.log('loder', this.props.loder);
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View style={{
                flex: 1,
                alignItems: "center"
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Profile'} />
                <PopUp isModalVisible={this.state.isModalVisible}
                    image={require('../../assets/Sucess.png')}
                    text="Success!"
                    text2="Your Profile has been Updated successfully !"
                    label="Ok"
                    Ok={() => this.setState({ isModalVisible: false })} />
                <PopUp isModalVisible={this.state.isModalVisible2}
                    image={require('../../assets/fail.png')}
                    text="Fail!"
                    text2="Something went wrong. Please try again."
                    label="Try again"
                    Ok={() => this.setState({ isModalVisible2: false })} />
                <View style={{ marginTop: verticalScale(-71) }} />
                <ScrollView>
                    <View style={{ width: scale(328), height: scale(490), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center" }}>

                        {!this.state.edit ?
                            <TouchableOpacity style={{ width: scale(57), height: scale(18), borderRadius: 8, borderColor: "#0093E9", borderWidth: 1, backgroundColor: "transparent", marginTop: verticalScale(10), alignSelf: "flex-end", marginRight: scale(10), flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                onPress={() => this._edit()}>
                                <Image
                                    style={{ width: scale(7.89), height: scale(7.89) }}
                                    source={require("../../assets/edit.png")}
                                    resizeMode='contain'
                                />
                                <Text style={{ color: "#0093E9", fontSize: scale(10), fontFamily: "Roboto-Regular", }}>EDIT</Text>
                            </TouchableOpacity> :

                            <View style={{ alignSelf: "flex-end", flexDirection: "column" }}>


                                <TouchableOpacity style={{ width: scale(57), height: scale(18), borderRadius: 8, backgroundColor: "transparent", marginTop: verticalScale(10), alignSelf: "flex-end", marginRight: scale(10), flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                    onPress={() => this._save()}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                        colors={['#0093E9', '#36C2CF']}
                                        style={{ width: scale(57), height: scale(18), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ color: "#FFFFFF", fontSize: scale(10), fontFamily: "Roboto-Bold", fontWeight: "bold" }}>SAVE</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ position: "relative", width: scale(57), height: scale(18), borderRadius: 8, borderColor: "#0093E9", borderWidth: 1, backgroundColor: "transparent", marginTop: verticalScale(10), alignSelf: "flex-end", marginRight: scale(10), flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                    onPress={() => this._cancel()}>
                                    <Text style={{ color: "#0093E9", fontSize: scale(10), fontFamily: "Roboto-Regular", }}>CANCEL</Text>
                                </TouchableOpacity>

                            </View>}
                        <View style={{ alignItems: "center", position: "absolute" }}>

                            <View style={{ marginTop: verticalScale(20), justifyContent: "center", alignItems: "center", width: scale(114), height: scale(122), }}>
                                <View style={{ flexDirection: "row", width: scale(114), height: scale(114), borderRadius: 200 }}>
                                    {/* <Image style={{ width: scale(91), height: scale(91), borderRadius: 400 }}
                                resizeMode='center'
                                source={this.state.image ?{uri:this.state.image} : require('../../assets/user.png')} /> */}
                                    <Image style={{ width: scale(114), height: scale(114), borderRadius: 400, }}
                                        resizeMode='cover'
                                        //source={{uri: 'data:image/jpeg;base64,' + this.state.filePath.data,}} />
                                        source={{ uri: user.profile_picture }} />
                                    {this.state.edit ?
                                        <TouchableOpacity activeOpacity={0.8} onPress={this.chooseFile.bind(this)}
                                            style={{ position: 'absolute', left: scale(75), alignSelf: "flex-start", width: scale(20), height: scale(20), borderRadius: 400, backgroundColor: "#FFFFFF", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}>
                                            <Image
                                                style={{ height: scale(9.79), width: scale(13.18) }}
                                                source={require('../../assets/edit.png')}
                                                resizeMode="center" />
                                        </TouchableOpacity>
                                        : null}
                                </View>

                                <Text style={{ color: "#0093E9", fontSize: scale(20), fontFamily: "Roboto-Bold", fontWeight: "bold", marginTop: verticalScale(7) }}>{this.state.name}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(12), marginLeft: scale(16), marginRight: scale(16), flex: 1, }}>
                                <View style={{ flexDirection: "row", alignItems: "center", alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Role</Text>
                                    {/* <TextInput style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", alignSelf: "center", height: scale(35), borderBottomWidth: this.state.edit? 1:0, borderBottomColor: this.state.edit? "#D6D6D6" : null }}
                                onChangeText={(role) => this.setState({ role })}
                                value={this.state.role} 
                                editable={this.state.edit}
                                />  */}

                                    <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", }}>{this.state.role}</Text>

                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(15), alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Organisation</Text>
                                    {/* <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%" }}>Bharat Biotech, IN</Text> */}
                                    <TextInput style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", alignSelf: "center", height: scale(35), borderBottomWidth: this.state.edit ? 1 : 0, borderBottomColor: this.state.edit ? "#D6D6D6" : null }}
                                        onChangeText={(organisation) => this.setState({ organisation })}
                                        value={this.state.organisation}
                                        editable={this.state.edit}
                                    />
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(15), height: scale(35), alignItems: "center" }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Affiliated Organisation</Text>
                                    <View style={{ flexDirection: "row", width: "55%" }}>
                                        <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular" }} numberOfLines={2}>{this.state.affiliateOrganisation}</Text>
                                    </View>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(15), alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Wallet Address:</Text>
                                    <Text style={{ color: "#0159EA", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", textDecorationLine: 'underline', textDecorationColor: "#0159EA" }} numberOfLines={1}>{this.state.address}</Text>
                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(15), alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Email</Text>
                                    {/* <TextInput style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", alignSelf: "center", height: scale(35), borderBottomWidth: this.state.edit? 1:0, borderBottomColor: this.state.edit? "#D6D6D6" : null }}
                                onChangeText={(email) => this.setState({ email })}
                                value={this.state.email} 
                                editable={this.state.edit}
                                />  */}
                                    <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", }}>{this.state.email}</Text>
                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(15), alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Phone</Text>
                                    {/* <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%" }}>+91 6396966723</Text> */}
                                    <TextInput style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%", alignSelf: "center", height: scale(35), borderBottomWidth: this.state.edit ? 1 : 0, borderBottomColor: this.state.edit ? "#D6D6D6" : null }}
                                        onChangeText={(phone) => this.setState({ phone })}
                                        value={this.state.phone}
                                        editable={this.state.edit}
                                    />
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(15), alignItems: "center", height: scale(35) }}>
                                    <Text style={{ color: "#707070", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "45%" }}>Account Status</Text>
                                    <Text style={{ color: "#000000", fontSize: scale(12), fontFamily: "Roboto-Regular", width: "55%" }}>{this.state.status ? "Active" : "Inactive"}</Text>
                                </View>

                            </View>
                        </View>
                    </View>

                    <View style={{ height: scale(10) }} />
                </ScrollView>

            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.userinfo.user,
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { upload_Image, user_update })(Profile)