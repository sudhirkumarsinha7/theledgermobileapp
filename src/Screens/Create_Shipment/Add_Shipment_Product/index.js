import React from 'react';
import {
    Button, View, Text, StyleSheet, BackHandler, ImageBackground, FlatList,
    Image, ScrollView, TextInput, TouchableOpacity, StatusBar, Animated,
    PermissionsAndroid,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import countries from "../../../Json/countries.json"
import { connect } from 'react-redux';
import { userinfo, upload_Image, user_update, selected_po } from '../../../Action';
import Picker from 'react-native-picker';
import CountryDropdown from "../../../components/CountryDropdown";
import PODropDown from "../../../components/PODropDown";
import DatePicker from 'react-native-datepicker';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import LinearGradient from 'react-native-linear-gradient';
//import countries from "all-countries-and-cities-json";

class Add_Shipment_Product extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.state = {
            loading: true,
            qrvalue: '',
            opneScanner: false,
            isModalVisible: false,
            isModalVisible2: false,

            shipmentId: props.navigation.getParam('shipmentId'),
            client: props.navigation.getParam('client'),
            POID: props.navigation.getParam('POID'),
            supplierLocation: props.navigation.getParam('supplierLocation'),
            deliveryLocation: props.navigation.getParam('deliveryLocation'),
            supplier: '',
            shipmentDate: props.navigation.getParam('shipmentDate'),
            estimateDeliveryDate: props.navigation.getParam('estimateDeliveryDate'),
            deliveryTo: props.navigation.getParam('deliveryTo'),
            productsdetails: props.navigation.getParam('data'),
            products: [{
                productName: "",
                manufacturerName: "",
                quantity: "",
                manufacturingDate: "",
                expiryDate: "",
                //storageCondition: "",
                batchNumber: "",
                serialNumber: "",
                storageConditionmax: "",
                storageConditionmin: ""

            }]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment_Details');
        return true;
    }
    componentDidMount = async () => {
        console.log("Add_Shipment_Product", this.state.products);
        this.setState({
            loading: false,
            products: this.state.productsdetails
        })
    }

    _createDateData() {
        let date = [];
        for (let i = 2016; i < 2050; i++) {
            let month = [];
            for (let j = 1; j < 13; j++) {
                let _month = {};
                _month[j] = j;
                month.push(j);
            }
            let _date = {};
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }

    _createCelsiusData() {
        let Celsius = [];
        for (let i = -30; i < 31; i++) {
            let left = [];
            for (let j = -30; j < 31; j++) {
                let _left = {};
                _left[j] = j;
                left.push(j + '°C');
            }
            let _right = {};
            _right[i + '°C'] = left;
            Celsius.push(_right);
        }
        return Celsius;
    }

    _showCelsiusPicker(index) {
        Picker.init({
            pickerData: this._createCelsiusData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Storage Conditions",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].storageConditionmin = pickedValue[0]
                productClone[index].storageConditionmax = pickedValue[1]
                this.setState({ products: productClone })

            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                // var date = pickedValue[0] + "  -  " + pickedValue[1]
                // const productClone = JSON.parse(JSON.stringify(this.state.products));
                // productClone[index].storageCondition = date
                // this.setState({products: productClone })
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].storageConditionmin = pickedValue[0]
                productClone[index].storageConditionmax = pickedValue[1]
                this.setState({ products: productClone })
            }
        });
        Picker.show();
    }

    _showMfgDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturing Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturingDate = date
                this.setState({ products: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturingDate = date
                this.setState({ products: productClone })
            }
        });
        Picker.show();
    }

    _showExpDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Expiry Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].expiryDate = date
                this.setState({ products: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].expiryDate = date
                this.setState({ products: productClone })
            }
        });
        Picker.show();
    }

    _showProductName(index) {
        Picker.init({
            pickerData: ['bOPV', 'MMR', 'PVC', 'BCG', 'RV', 'Hep B'],
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Product",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].productName = value
                this.setState({ products: productClone })

            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].productName = value
                this.setState({ products: productClone })
            }
        });
        Picker.show();
    }

    _showManufacturer(index) {
        Picker.init({
            pickerData: ['Bharat Biotech', 'Aroma Biotech', 'Chronly industries', 'GH Pharmas',],
            pickerTextEllipsisLen: 30,
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturer",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturerName = value
                this.setState({ products: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.products));
                productClone[index].manufacturerName = value
                this.setState({ products: productClone })

            }
        });
        Picker.show();
    }

    _add_another_product() {
        this.setState({
            products: [...this.state.products, {
                productName: '',
                manufacturerName: '',
                quantity: '',
                manufacturingDate: "",
                expiryDate: "",
                //storageCondition: "",
                batchNumber: "",
                serialNumber: "",
                storageConditionmax: "",
                storageConditionmin: ""
            }]
        })
    }

    updateValue(index, item) {
        const name = Object.keys(item);
        if (name[0] === "batchNumber") {
            if (!/[^a-zA-Z0-9-]/.test(item.batchNumber)) {
                this.setState({
                    products: [
                        ...this.state.products.slice(0, index),
                        Object.assign({}, this.state.products[index], item),
                        ...this.state.products.slice(index + 1)
                    ]
                });
            } else {
                alert('Batch Number Does not Accept Special Characters')
            }
        } else if (name[0] === "serialNumber") {
            if (!/[^a-zA-Z0-9-]/.test(item.serialNumber)) {
                this.setState({
                    products: [
                        ...this.state.products.slice(0, index),
                        Object.assign({}, this.state.products[index], item),
                        ...this.state.products.slice(index + 1)
                    ]
                });
            } else {
                alert('Serial Number Does not Accept Special Characters')
            }
        } else if (name[0] === "quantity") {
            const re = /^[0-9\b]+$/;
            // if (parseInt(item.quantity) > 0) {
            if (parseInt(item.quantity) > 0 && re.test(item.quantity)) {

                this.setState({
                    products: [
                        ...this.state.products.slice(0, index),
                        Object.assign({}, this.state.products[index], item),
                        ...this.state.products.slice(index + 1)
                    ]
                });
            } else {
                alert('Invalid Quantity')
            }
        }
        // this.setState({
        //     products: [
        //         ...this.state.products.slice(0, index),
        //         Object.assign({}, this.state.products[index], item),
        //         ...this.state.products.slice(index + 1)
        //     ]
        // });
        // console.log('products', this.state.products);
    }

    review_shipment = async () => {
        console.log('save', this.state.products);
        const {
            shipmentId,
            client,
            POID,
            supplierLocation,
            deliveryLocation,
            supplier,
            shipmentDate,
            estimateDeliveryDate,
            deliveryTo,
        } = this.state;
        // || !res.manufacturingDate || !res.expiryDate || !res.batchNumber || !res.serialNumber || !res.storageConditionmax || !res.storageConditionmin
        // for (let index in this.state.products) {
        //     let res = this.state.products[index]
        //     if (!res.productName || !res.manufacturerName || !res.quantity) {
        //         alert('Please fill all the details to Proceed')
        //         throw new Error('inventory unsuccessful')
        //     }
        // }
        const receiver = this.props.alluserdata.find(usr => usr.name === deliveryTo);
        console.log("ree",receiver);
        const data = {
            shipmentId,
            client,
            receiver: receiver.address,
            supplier:this.props.user.name,
            supplierLocation,
            shipmentDate,
            deliveryTo,
            deliveryLocation,
            estimateDeliveryDate,
            status: 'Shipped',
            products:this.state.products
        };
        console.log("data,",data,POID);
        this.props.navigation.navigate('Review_Shipment', {
            POID,
            data
        })
    }

    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('Inventory')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }
    cancel = (index) => {
        console.log("hello", index);
        const productClone = JSON.parse(JSON.stringify(this.state.products));
        console.log(Object.keys(productClone));
        productClone.splice(index, 1)
        this.setState({ products: productClone })
    }
    AsyncAlert(message) {
        return new Promise((resolve, reject) => {
            Alert.alert(
                '',
                message,
                [
                    { text: 'OK', onPress: () => resolve('YES') },
                ],
                { cancelable: false }
            )
        })
    }
    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    onBarcodeScan(qrvalue) {
        //called after te successful scanning of QRCode/Barcode
        const productClone = JSON.parse(JSON.stringify(this.state.products));
        const index = this.state.index;

        if (!this.isJson(qrvalue)) {
            this.AsyncAlert("invalid QR Code").then(() => {
                this.setState({ opneScanner: true })
            })
            return
        }

        const value = JSON.parse(qrvalue);
        console.log('ggg', value);
        productClone[index].manufacturingDate = value.mfg
        productClone[index].expiryDate = value.exp
        productClone[index].storageConditionmax = value.s_max
        productClone[index].storageConditionmin = value.s_min
        productClone[index].batchNumber = value.batchno
        productClone[index].serialNumber = value.serialno

        this.setState({ products: productClone })
        this.setState({ qrvalue: qrvalue });
        this.setState({ opneScanner: false });
    }

    onOpneScanner(i) {
        var that = this;
        //To Start Scanning
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'CameraExample App Camera Permission',
                        'message': 'CameraExample App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //If CAMERA Permission is granted
                        console.log("start");
                        that.setState({ index: i });
                        that.setState({ qrvalue: '' });
                        that.setState({ opneScanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    console.log("vef", err);

                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            //Calling the camera permission function
            requestCameraPermission();
        } else {
            console.log("start");

            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
        }
    }


    render() {
        var Products_Details = [];
        for (let i = 0; i < this.state.products.length; i++) {
            Products_Details.push(
                <View style={{ marginTop: verticalScale(20), width: scale(328), height: scale(300), backgroundColor: "#FFFFFF", borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                    <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>

                        <View style={{ flexDirection: "row", height: scale(35), marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Product Name</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showProductName(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].productName === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].productName === "" ? "Select Product" : this.state.products[i].productName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Manufacturer</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showManufacturer(i)}
                            >
                                <Text style={{ width: "85%", color: this.state.products[i].manufacturerName === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].manufacturerName === "" ? "Select Manufacturer" : this.state.products[i].manufacturerName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Quantity</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Quantity"
                                    keyboardType="number-pad"
                                    value={this.state.products[i].quantity}
                                    onChangeText={(quantity) => this.updateValue(i, { quantity })} />
                            </View>

                        </View>

                        {/* <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Mfg Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showMfgDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].manufacturingDate === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].manufacturingDate === "" ? "MM/YYYY" : this.state.products[i].manufacturingDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>


                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Exp Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showExpDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].expiryDate === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].expiryDate === "" ? "MM/YYYY" : this.state.products[i].expiryDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Storage Conditions</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={() => this._showCelsiusPicker(i)}>
                                <Text style={{ width: "85%", color: this.state.products[i].storageConditionmin === "" || this.state.products[i].storageConditionmax === "" ? "#A8A8A8" : "#000" }}>{this.state.products[i].storageConditionmin === "" && this.state.products[i].storageConditionmax === "" ? "Enter Storage Conditions" : this.state.products[i].storageConditionmin + " to " + this.state.products[i].storageConditionmax}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Batch Number</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Batch Number"
                                    value={this.state.products[i].batchNumber}
                                    onChangeText={(batchNumber) => this.updateValue(i, { batchNumber })} />
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Serial Numbers</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Serial Numbers"
                                    value={this.state.products[i].serialNumber}
                                    onChangeText={(serialNumber) => this.updateValue(i, { serialNumber })} />
                            </View>

                        </View> */}


                        <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
                            <TouchableOpacity
                                style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), marginRight: scale(20), borderRadius: 8, borderWidth: 1, borderColor: this.state.products.length === 1 ? "#D6D6D6" : "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}
                                onPress={() => this.cancel(i)}
                                disabled={this.state.products.length === 1 ? true : false}
                            >
                                <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: this.state.products.length === 1 ? "#D6D6D6" : "#0093E9", fontWeight: "bold" }}>CANCEL</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}
                                onPress={() => this.onOpneScanner(i)}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={['#0093E9', '#36C2CF']}
                                    style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}>
                                    <Image style={{ width: scale(15), height: scale(15) }}
                                        resizeMode='center'
                                        source={require('../../../assets/Scan.png')} />
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SCAN</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>)
        }
        if (!this.state.opneScanner) {
            return (
                <View style={styles.container}>

                    {/* {this.props.loder && <LottieView source={require('../../../Json/loading.json')} autoPlay loop />} */}
                    <StatusBar backgroundColor="#0093E9" />
                    <Header name={'Create Shipment'} />
                    <ScrollView nestedScrollEnabled={true}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-75) }}>

                        {Products_Details}

                        <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(46), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                            onPress={() => this._add_another_product()}>

                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../../assets/plus.png')} />
                            <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Add Another Product</Text>
                        </TouchableOpacity>

                        <View style={{ width: scale(328), height: scale(40), backgroundColor: "transparent", marginTop: verticalScale(18), flexDirection: "row", justifyContent: "space-between" }}>
                            <TouchableOpacity style={{ width: scale(149), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", borderColor: "#0093E9", borderWidth: 1, flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                onPress={() => this.handleBackButtonClick()}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>BACK</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: scale(149), height: scale(40), borderRadius: 10, backgroundColor: "#FFAB1D", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                onPress={() => this.review_shipment()}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={['#0093E9', '#36C2CF']}
                                    style={{ width: scale(149), height: scale(40), borderRadius: 10, backgroundColor: "#FFAB1D", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }} >
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>REVIEW SHIPMENT</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: scale(25) }} />

                    </ScrollView>
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <CameraKitCameraScreen
                    showFrame={true}
                    //Show/hide scan frame
                    scanBarcode={true}
                    //Can restrict for the QR Code only
                    laserColor={'green'}
                    //Color can be of your choice
                    frameColor={'green'}
                    //If frame is visible then frame color
                    colorForScannerFrame={'black'}
                    //Scanner Frame color
                    onReadCode={event =>
                        this.onBarcodeScan(event.nativeEvent.codeStringValue)
                    }
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})
function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        loder: state.loder,
        poid: state.purchaseorder.purchaseorderids

    }
}

export default connect(mapStateToProps, { upload_Image, user_update, selected_po })(Add_Shipment_Product)
// export default Create_Purchase_Order;