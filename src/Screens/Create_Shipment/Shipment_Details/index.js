import React from 'react';
import {
    Button, View, Text, StyleSheet, BackHandler, ImageBackground, FlatList,
    Image, ScrollView, TextInput, TouchableOpacity, StatusBar, Animated
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import countries from "../../../Json/countries.json"
import { connect } from 'react-redux';
import { userinfo, upload_Image, user_update, selected_po } from '../../../Action';
import Picker from 'react-native-picker';
import CountryDropdown from "../../../components/CountryDropdown";
import PODropDown from "../../../components/PODropDown";
import DatePicker from 'react-native-datepicker';
import LinearGradient from 'react-native-linear-gradient';
//import countries from "all-countries-and-cities-json";

class Shipment_Details extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.state = {
            deliveryLocationisModalVisible: false,
            text: '',
            POID: props.navigation.getParam('id', ''),
            shipmentId: '',
            client: '',
            supplierisModalVisible: false,
            supplierLocation: '',
            deliveryLocation: '',
            shipmentDate: '',
            estimateDeliveryDate: '',
            deliveryTo: '',
            products: []
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home_Shipment');
        return true;
    }
    componentDidMount = async () => {
        console.log("Create_Purchase_Order", this.state.POID, countries);
        if (this.state.POID !== '') {
            const id = this.state.POID
            const result = await this.props.selected_po(id.toString());
            const poDetail = JSON.parse(result[result.length - 1].data)
            this.setState({
                deliveryTo: poDetail.receiver.name,
                deliveryLocation: poDetail.destination,
                products: poDetail
            })
        }
        let data = [];
        for (let key in countries) {
            const Object = {};
            countries[key].forEach((item, index) => {
                data.push(item + ", " + key);
            })

        }
        console.log("data", data);

        this.arrayholder = data;
        this.setState({
            countries: data,
            final: data
        })
    }
    _user() {
        let alluserdata = this.props.alluserdata
        let user = [];
        for (var i = 0; i < alluserdata.length; i++) {
            if (this.props.user.name !== alluserdata[i].name) {
                user.push(alluserdata[i].name)
            }
        }
        return user
    }
    _showUserName() {
        Picker.init({
            pickerData: this._user(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Delivery To",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                this.setState({ deliveryTo: pickedValue })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                this.setState({ deliveryTo: pickedValue })
            }
        });
        Picker.show();
    }

    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item ? item.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });

        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            countries: newData,
            text: text,
        });
    }

    updateValue(index, item) {
        this.setState({
            products: [
                ...this.state.products.slice(0, index),
                Object.assign({}, this.state.products[index], item),
                ...this.state.products.slice(index + 1)
            ]
        });
        console.log('data', this.state.products);
    }

    onSelectPO = async (item) => {
        this.setState({ POID: item, PODropDownisModalVisible: false })
        const result = await this.props.selected_po(item);

        const poDetail = JSON.parse(result[result.length - 1].data)
        console.log("result", poDetail);
        this.setState({
            deliveryTo: poDetail.receiver.name,
            deliveryLocation: poDetail.destination,
            products: poDetail
        })
    }

    _add_product = () => {
        const {
            POID,
            shipmentId,
            client,
            supplierLocation,
            deliveryLocation,
            shipmentDate,
            estimateDeliveryDate,
            deliveryTo
        } = this.state
        //!POID || 
        if ( !shipmentId || !client || !supplierLocation || !shipmentDate || !deliveryLocation || !estimateDeliveryDate || !deliveryTo) {
            alert('Please fill all the details to Proceed')
        }
        else {
            const productsdetails = this.state.products;
            console.log(productsdetails);
            let data = [];
            if (productsdetails.length !== 0) {
                for (let key in productsdetails.products) {
                    const object = {}
                    Object.keys(productsdetails.products[key]).forEach(element => {
                        object['productName'] = element.split('-')[0],
                            object['manufacturerName'] = element.split('-')[1],
                            object['quantity'] = productsdetails.products[key][element],
                            object['storageConditionmin'] = "",
                            object['storageConditionmax'] = "",
                            object['manufacturingDate'] = "",
                            object['expiryDate'] = "",
                            object['batchNumber'] = "",
                            object['serialNumber'] = ""
                    });
                    data.push(object)
                }
            } else {
                const object = {}
                object['productName'] = "",
                            object['manufacturerName'] = "",
                            object['quantity'] = "",
                            object['storageConditionmin'] = "",
                            object['storageConditionmax'] = "",
                            object['manufacturingDate'] = "",
                            object['expiryDate'] = "",
                            object['batchNumber'] = "",
                            object['serialNumber'] = ""
                            data.push(object)
                console.log("hello",data);
            }
            console.log("state2", data,POID)
            this.props.navigation.navigate('Add_Shipment_Product', {
                POID,
                shipmentId,
                client,
                supplierLocation,
                deliveryLocation,
                shipmentDate,
                estimateDeliveryDate,
                deliveryTo,
                data
            })
        }

    }

    render() {
        console.log("podata",this.props.poid);
        return (
            <View style={styles.container}>

                {/* {this.props.loder && <LottieView source={require('../../../Json/loading.json')} autoPlay loop />} */}
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Create Shipment'} />
                <CountryDropdown
                    isModalVisible={this.state.deliveryLocationisModalVisible}
                    countries={this.state.countries}
                    text={this.state.text}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onPress={(item) => this.setState({ deliveryLocation: item, deliveryLocationisModalVisible: false, text: '', countries: this.state.final })}
                />
                <CountryDropdown
                    isModalVisible={this.state.supplierisModalVisible}
                    countries={this.state.countries}
                    text={this.state.text}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onPress={(item) => this.setState({ supplierLocation: item, supplierisModalVisible: false, text: '', countries: this.state.final })}
                />
                <PODropDown
                    PODropDownisModalVisible={this.state.PODropDownisModalVisible}
                    podata={this.props.poid}
                    //onPress={(item) => this.setState({ POID: item, PODropDownisModalVisible: false })}
                    onPress={(item) => this.onSelectPO(item)}
                />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-75) }}>

                    <View style={{ height: scale(500), width: scale(328), backgroundColor: "#FFFFFF", marginTop: verticalScale(10), borderRadius: 10 }}>
                        <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>

                            <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Shipment ID</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Enter Shipment ID"

                                        value={this.state.shipmentId}
                                        onChangeText={(shipmentId) => this.setState({ shipmentId })}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Client</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Enter Client"

                                        value={this.state.client}
                                        onChangeText={(client) => this.setState({ client })}
                                    />
                                </View>
                            </View>


                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Purchase Order ID</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this.setState({ PODropDownisModalVisible: true })}
                                    disabled={this.props.poid.length === 0 ? true : false}>
                                    <Text style={{ width: "85%", color: this.state.POID === "" ? "#A8A8A8" : "#000" }}>{this.state.POID === "" ? "Select Purchase Order" : this.state.POID}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Supplier</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}>
                                    <Text style={{ width: "85%", color: "#000" }}>{this.props.user.name}</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Supplier Location</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this.setState({ supplierisModalVisible: true })}>
                                    <Text style={{ width: "85%", color: this.state.supplierLocation === "" ? "#A8A8A8" : "#000" }} numberOfLines={1}>{this.state.supplierLocation === "" ? "Select Delivery location" : this.state.supplierLocation}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Shipment Date</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}>
                                    <DatePicker
                                        style={{
                                            height: scale(35),
                                            width: "95%",
                                        }}
                                        date={this.state.shipmentDate} //initial date from state
                                        mode="date" //The enum of date, datetime and time
                                        placeholder="DD/MM/YYYY"
                                        placeHolderTextStyle={{ color: "#707070", fontSize: scale(15), }}
                                        format="DD/MM/YYYY"
                                        minDate="01-01-1900"
                                        maxDate="01-01-2100"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image
                                            style={{ width: scale(14.04), height: scale(14.04), }}
                                            source={require("../../../assets/calendar.png")}
                                            resizeMode='contain'
                                        />}
                                        customStyles={{
                                            dateInput: {
                                                borderWidth: 0,
                                                fontSize: scale(15),
                                                height: scale(50),
                                                width: "100%",
                                                alignItems: "flex-start",


                                            }
                                        }}
                                        onDateChange={(date) => { this.setState({ shipmentDate: date }) }}
                                    />

                                </View>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Delivery To</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this._showUserName()}>
                                    <Text style={{ width: "85%", color: this.state.deliveryTo === "" ? "#A8A8A8" : "#000" }}>{this.state.deliveryTo === "" ? "Select Person" : this.state.deliveryTo}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>


                            <View style={{ flexDirection: "row", height: scale(30), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20), }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Delivery location</Text>
                                </View>

                                <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                    onPress={() => this.setState({ deliveryLocationisModalVisible: true })}>
                                    <Text style={{ width: "85%", color: this.state.deliveryLocation === "" ? "#A8A8A8" : "#000" }} numberOfLines={1}>{this.state.deliveryLocation === "" ? "Select Delivery location" : this.state.deliveryLocation}</Text>
                                    <Image
                                        style={{ width: scale(10.2), height: scale(14.09), }}
                                        source={require("../../../assets/updown.png")}
                                        resizeMode='contain'
                                    />
                                </TouchableOpacity>
                            </View>

                            <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(20) }}>
                                <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                    <Text style={{ color: "#707070", }}>Estimate Delivery Date</Text>
                                </View>

                                <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}>
                                    <DatePicker
                                        style={{
                                            height: scale(35),
                                            width: "95%",
                                        }}
                                        date={this.state.estimateDeliveryDate} //initial date from state
                                        mode="date" //The enum of date, datetime and time
                                        placeholder="DD/MM/YYYY"
                                        placeHolderTextStyle={{ color: "#707070", fontSize: scale(15), }}
                                        format="DD/MM/YYYY"
                                        minDate={this.state.shipmentDate ? this.state.shipmentDate : "01-01-1900"}
                                        maxDate="01-01-2100"
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        iconComponent={<Image
                                            style={{ width: scale(14.04), height: scale(14.04), }}
                                            source={require("../../../assets/calendar.png")}
                                            resizeMode='contain'
                                        />}
                                        customStyles={{
                                            dateInput: {
                                                borderWidth: 0,
                                                fontSize: scale(15),
                                                height: scale(50),
                                                width: "100%",
                                                alignItems: "flex-start",


                                            }
                                        }}
                                        onDateChange={(date) => { this.setState({ estimateDeliveryDate: date }) }}
                                    />

                                </View>
                            </View>

                        </View>
                    </View>



                    <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(46), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                        onPress={() => this._add_product()}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#0093E9', '#36C2CF']}
                            style={{ width: scale(328), height: scale(46), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }} >
                            <Image style={{ width: scale(13.73), height: scale(13.73), }}
                                resizeMode='center'
                                source={require('../../../assets/add.png')} />
                            <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Add Product</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <View style={{ height: scale(25) }} />

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})
function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        loder: state.loder,
        poid: state.purchaseorder.purchaseorderids.reverse()

    }
}

export default connect(mapStateToProps, { upload_Image, user_update, selected_po })(Shipment_Details)
// export default Create_Purchase_Order;