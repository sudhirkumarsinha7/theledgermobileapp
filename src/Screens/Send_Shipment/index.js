import React from 'react';
import {
    Button, View, Text, StyleSheet, ScrollView, BackHandler,
    TextInput, ImageBackground, Image, TouchableOpacity, StatusBar, Animated,
    KeyboardAvoidingView
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import { add_inventory, Create_Purchase_Order } from '../../Action';
import Header from "../../components/Header"
import Picker from 'react-native-picker';
import PopUp from "../../components/PopUp";
import { customisepurchaseorderData } from "../../utils/Purchase_Order_Data";
import LinearGradient from 'react-native-linear-gradient';
import { create_new_shipmemnt, fetchPublisherShipments } from "../../Action"

class Send_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shipment_data: this.props.navigation.getParam('data'),
            mydata: {
                deliveryTo: '',
                deliveryLocation: '',
                //quantity: '',
                status: 'Shipped',
                shipmentId: '',
                receiver: '',
                sender: this.props.user?.address,
                //serialNumberRange:'',
                products: []
            },

        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home_Shipment');
        return true;
    }
    componentDidMount = async () => {
        console.log("dddd", this.props.navigation.getParam('data'));
        this.setState({
            mydata: {
                ...this.state.mydata, products: this.state.shipment_data?.products,
                // quantity: this.state.shipment_data?.quantity,
                // serialNumberRange: this.state.shipment_data?.serialNumberRange || this.state.shipment_data?.products[0].serialNumber 
            }
        })
    }

    update = (val, name) => {
        console.log("this", this.state.mydata);
        if (name === "deliveryLocation") {
            this.setState({
                mydata: {
                    ...this.state.mydata, deliveryLocation: val
                }
            })
        } else if (name === "deliveryTo") {
            this.setState({
                mydata: {
                    ...this.state.mydata, deliveryTo: val,
                }
            })
        } else if (name === "quantity") {
            this.setState({
                mydata: {
                    ...this.state.mydata, quantity: val
                }
            })
        } else if (name === "shipmentId") {
            this.setState({
                mydata: {
                    ...this.state.mydata, shipmentId: val
                }
            })
        } else if (name === "serialNumberRange") {
            this.setState({
                mydata: {
                    ...this.state.mydata, serialNumberRange: val
                }
            })
        }
    }

    sendShipment = async () => {
        const data = { ...this.state.shipment_data, ...this.state.mydata }

        console.log("data", data,this.state.mydata);
        const result = await this.props.create_new_shipmemnt(data)
        console.log("result ship", result);
        if (result.status === 200) {
            const res = await this.props.fetchPublisherShipments(0,5)
            console.log("fetchPublisherShipments",res);
            if (res.status === 200) {
                this.props.navigation.navigate('Home_Shipment')
            }
        } else {
            alert("something is wrong")
        }
    }

    _user() {
        console.log("this.props.alluserdata", this.props.alluserdata);
        let alluserdata = this.props.alluserdata
        let user = [];
        for (var i = 0; i < alluserdata.length; i++) {
            if (this.props.user.name !== alluserdata[i].name) {
                user.push(alluserdata[i].name)
            }
        }
        console.log("user", user);
        return user
    }

    _showUserName() {
        Picker.init({
            pickerData: this._user(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Delivery To",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                console.log("pickedValue", pickedValue);
                // this.setState({ deliveryTo: pickedValue })
                const receiver = this.props.alluserdata.find(usr => usr.name === pickedValue[`0`]);
                console.log("receiver", receiver);
                this.setState({
                    mydata: {
                        ...this.state.mydata, deliveryTo: pickedValue[`0`], receiver: receiver.address

                    }
                })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                // this.setState({ deliveryTo: pickedValue })
                const receiver = this.props.alluserdata.find(usr => usr.name === pickedValue[`0`]);
                this.setState({
                    mydata: {
                        ...this.state.mydata, deliveryTo: pickedValue[`0`], receiver: receiver.address,
                    }
                })
            }
        });
        Picker.show();
    }

    updateArray = (item) => {
        // this.setState({
        //     bore_details_value: [
        //         ...this.state.bore_details_value.slice(0, index),
        //         Object.assign({}, this.state.bore_details_value[index], item),
        //         ...this.state.bore_details_value.slice(index + 1)
        //     ]
        // });
        console.log("item",item);
        this.setState({
            mydata: {
                 ...this.state.mydata,
                products: [
                    ...this.state.mydata.products.slice(0, 0),
                    Object.assign({}, this.state.mydata.products[0], item),
                    ...this.state.mydata.products.slice(0 + 1)
                ]

            }
        });
    }

    render() {

        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Send Shipment'} />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-70) }}>
                    <View style={{ width: scale(328), flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20) }}>
                        <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                            <View style={{ flexDirection: "row", alignItems: "center", }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Shipment ID</Text>
                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Enter Shipment Id"

                                        value={this.state.mydata.shipmentId}
                                        onChangeText={(val) => this.update(val, "shipmentId")}
                                    />
                                </View>
                                {/* <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.shipment_data?.shipmentId}</Text> */}
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Deliver To</Text>
                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TouchableOpacity style={{ width: "100%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                        onPress={() => this._showUserName()}>
                                        <Text style={{ width: "85%", color: this.state.mydata?.deliveryTo === "" ? "#A8A8A8" : "#000" }}>{this.state.mydata?.deliveryTo === "" ? "Select Person" : this.state.mydata?.deliveryTo}</Text>
                                        <Image
                                            style={{ width: scale(10.2), height: scale(14.09), }}
                                            source={require("../../assets/updown.png")}
                                            resizeMode='contain'
                                        />
                                    </TouchableOpacity>
                                    {/* <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Deliver To"
                                        //keyboardType="number-pad"
                                        value={this.state.mydata.deliveryTo}
                                        onChangeText={(val) => this.update(val, "deliveryTo")}
                                    /> */}
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Delivery Location</Text>
                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Location"
                                        //keyboardType="number-pad"
                                        value={this.state.mydata.deliveryLocation}
                                        onChangeText={(val) => this.update(val, "deliveryLocation")}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{ height: 10 }} />
                    </View>

                    <View style={{ width: scale(328), flex: 1, backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20) }}>
                        <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                            <View style={{ flexDirection: "row", alignItems: "center", }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>{this.state.shipment_data?.products[0].productName || Object.keys(this.state.shipment_data?.products[0])[0].split('-')[0]}</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}></Text>
                            </View>
                            {/* <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Manufacturer</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}></Text>
                            </View> */}
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Batch No</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.shipment_data?.batchNumber || this.state.shipment_data?.products[0].batchNumber}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Quantity</Text>
                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="qty"
                                        keyboardType="number-pad"
                                        value={this.state.mydata?.products[0]?.quantity?.toString()}
                                        onChangeText={(quantity) => this.updateArray({ quantity })}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Serial No.</Text>
                                <View style={{ width: "50%", height: scale(35), borderBottomColor: "#E8E8E8" }}>
                                    <KeyboardAvoidingView>
                                    <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                        placeholder="Serial No."
                                        keyboardType="number-pad"
                                        value={this.state.mydata?.products[0]?.serialNumber}
                                        onChangeText={(serialNumber) => this.updateArray({serialNumber})}
                                    />
                                    </KeyboardAvoidingView>
                                </View>
                            </View>
                            {/* <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Serial No.</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.shipment_data?.serialNumberRange || this.state.shipment_data?.products[0].serialNumber}</Text>
                            </View> */}
                        </View>
                        <View style={{ height: 10 }} />
                    </View>


                </ScrollView>
                <KeyboardAvoidingView>
                </KeyboardAvoidingView>
                <View style={{ flexDirection: "row", width: scale(328), height: scale(40), borderRadius: 8,  position: 'relative', bottom: 10, justifyContent: "space-between" }}>

                    <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}
                        onPress={() => this.props.navigation.navigate('Home_Shipment')}>

                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold", }}>CANCEL</Text>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => { this.sendShipment() }}
                        style={{ width: scale(195), height: scale(40), borderRadius: 8, flexDirection: "row", backgroundColor: "#FA7923", alignItems: "center", justifyContent: "space-evenly" }}>

                        <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SEND SHIPMENT</Text>

                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }
}

// {
//     "orderID": "PO45163183",
//         "shipmentNumber": "SHP1345567",
//             "serialNumberRange": "SL1-SL1000",
//                 "manufacturingDate": "4/2/2020",
//                     "expiryDate": "4/2/2025",
//                         "itemNumber": "12",
//                             "materialCode": "S359190",
//                                 "quantity": "1000",
//                                     "batchNumber": "BB1234",
//                                         "newShipment": true
// }
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})

function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        inventorydata: state.inventory.inventorydata
    }
}

export default connect(mapStateToProps, { create_new_shipmemnt, fetchPublisherShipments })(Send_Shipment)

//export default Review_Inventory;