import React from 'react';
import { Button, View, Text, StyleSheet, ScrollView, BackHandler, ImageBackground, Image, TouchableOpacity, StatusBar, Animated } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';
import { add_inventory, Create_Purchase_Order } from '../../../Action';
import Header from "../../../components/Header"
import PopUp from "../../../components/PopUp";

class Review_Purchase_Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: props.navigation.getParam('products'),
            delivery_to: props.navigation.getParam('delivery_to'),
            destination: props.navigation.getParam('destination'),
            isModalVisible: false,
            isModalVisible2: false,
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Create_Purchase_Order');
        return true;
    }
    componentDidMount = async () => {
        console.log("Review Purchase Order", this.state.destination, this.state.delivery_to, this.state.products);
    }

    ok = () => {
        this.setState({ isModalVisible: false })
        this.props.navigation.navigate('Home_Shipment')
    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }
    _SaveProduct = async () => {
        const deliveryTo = this.props.alluserdata.find(usr => usr.name === this.state.delivery_to[0]);
        const { user } = this.props;
        let products = [];
        for (let key in this.state.products) {
            const object = {};
            object[`${this.state.products[key].productName}-${this.state.products[key].manufacturerName}`] = this.state.products[key].quantity
            products.push(object)

        }
        var d = new Date();
        const date = d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear();

        const data = {
                receiver: deliveryTo,
                supplier: { name: user.name, email: user.email },
                destination: this.state.destination,
                products: products,
                date: date,
        };
        console.log("final data", data);
        const result = await this.props.Create_Purchase_Order(data)
        console.log("result", result);
        if (result.status === 200) {
            this.setState({
                po_id: result.data.orderID,
                isModalVisible: true
            })
        } else {
            this.setState({ isModalVisible2: true })
        }

    }

    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Review Purchase Order'} />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-70) }}>
                    <PopUp isModalVisible={this.state.isModalVisible}
                        image={require('../../../assets/Sucess.png')}
                        text="Success!"
                        text2={"Your PurchaseOrder "+ this.state.po_id +" Created Successfully!"}
                        label="Ok"
                        Ok={() => this.ok()} />
                    <PopUp isModalVisible={this.state.isModalVisible2}
                        image={require('../../../assets/fail.png')}
                        text="Fail!"
                        text2="Something went wrong. Please try again."
                        label="Try again"
                        Ok={() => this.fail()} />

                    <View style={{ width: scale(328), height: scale(130), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(14) }}>
                        <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                            <View style={{ flexDirection: "row", alignItems: "center", }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Supplier</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.props.user.name}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Delivery To</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.delivery_to}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Delivery location</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{this.state.destination}</Text>
                            </View>
                        </View>
                    </View>

                    {this.state.products.map((item, index) => (
                        <View style={{ width: scale(328), height: scale(130), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20) }}>
                            <View style={{ marginLeft: scale(16), marginRight: scale(16), marginTop: verticalScale(19), flex: 1 }}>
                                <View style={{ flexDirection: "row", alignItems: "center", }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Product Name</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.productName}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Manufacturer</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.manufacturerName}</Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(19) }}>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", width: "45%" }}>Quantity</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.quantity}</Text>
                                </View>
                            </View>
                        </View>
                    ))}


                </ScrollView>
                <View style={{ height: scale(60) }} />
                <View style={{ flexDirection: "row", width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(20), position: 'absolute', bottom: 10, justifyContent: "space-between" }}>
                    {/* position: 'absolute', bottom: 10, */}
                    <TouchableOpacity style={{ width: scale(111), height: scale(40), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "center", alignItems: "center", borderColor: "#0093E9", borderWidth: 1 }}
                        //disabled={true}
                        onPress={() => this.props.navigation.navigate('Create_Purchase_Order')}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../assets/edit.png')} />
                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold", marginLeft: scale(10) }}>EDIT</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this._SaveProduct()}
                        style={{ width: scale(195), height: scale(40), borderRadius: 8, flexDirection: "row", backgroundColor: "#FA7923", alignItems: "center", justifyContent: "space-evenly" }}>
                        <Image style={{ width: scale(13.14), height: scale(15), }}
                            resizeMode='center'
                            source={require('../../../assets/Purchaseorder.png')} />
                        <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Create</Text>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1
    },
})

function mapStateToProps(state) {
    return {
        alluserdata: state.alluser.alluserdata,
        user: state.userinfo.user,
        inventorydata: state.inventory.inventorydata,
    }
}

export default connect(mapStateToProps, { add_inventory, Create_Purchase_Order })(Review_Purchase_Order)

//export default Review_Inventory;