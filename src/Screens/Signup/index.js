import React from 'react';
import { saveUser } from '../../Action'
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    StatusBar,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import FloatingLabelInput from "../../components/FloatingLabelInput";
import LinearGradient from 'react-native-linear-gradient';
import PopUp from "../../components/PopUp";

const userinfo = { name: 'admin', pass: 'admin', email: 'sh@gmail.com' };
const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
}
class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            name: '',
            Email: '',
            pass: '',
            hidePassword: true,
            error: null,
            error_name: null,
            error_email: null,
            error_pwd: null,
            error_email_line: false,
            error_name_line: false,
            error_pwd_line: false,
            isModalVisible: false,
            isModalVisible2: false
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount() {
        console.log('signup', this.props.userdata);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Auth');
        return true;
    }
    validateEmail() {
        let reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // /^([a-z0-9\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
        // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg_email.test(this.state.Email);
    }
    validatePassword() {
        let reg_pwd = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
        return reg_pwd.test(this.state.pass);
    }
    _signup = async () => {
        var {
            name,
            pass,
            Email,
        } = this.state
        const data = {
            "name": name,
            "email": Email,
            "password": pass
        }
        if (name == "" && pass == "") this.setState({ error_name: "Enter your Name", error_email: "Enter your Email", error_pwd: "Enter your Password", error_name_line: true, error_email_line: true, error_pwd_line: true })
        else if (name == "") this.setState({ error_name: "Enter your Name", error_name_line: true, error_email: null, error_pwd: null, error_pwd_line: false, error_email_line: false, });
        else if (!this.validateEmail()) {
            this.setState({ error_email: "Incorrect Email Format", error_email_line: true, error_name: null, error_name_line: false, error_pwd: null, error_pwd_line: false, });
        }
        else if (pass == "")
            this.setState({ error_pwd: "Enter your Password", error_pwd_line: true, error_email: null, error_email_line: false, error_name: null, error_name_line: false, });
        else if (!this.validatePassword())
            this.setState({
                error_pwd: "Password must contain: one capital letter, one number, one special character & a minimum of 8 characters",
                error_pwd_line: true, error_email: null, error_email_line: false, error_name: null, error_name_line: false,
            });
        else {
            const result = await this.props.saveUser(data)
            if (result.status === 200) {
                this.props.navigation.navigate('OTP')
            } else if (result.data.data[0].msg === "E-mail already in use") {
                this.setState({
                    isModalVisible: true
                })
            }
            else {
                this.setState({
                    isModalVisible2: true
                })
            }
            // const result = await this.props.saveUser(data);
            // if (result.status === 200) {
            //     this.props.navigation.navigate('OTP')
            // } else {

            //     const err = result.data.data[0];
            //     console.log('eerrrr',err);

            //     alert(err.msg);
            // }
            //alert('logged');
            // this.props.saveUser(data, () => this.props.navigation.navigate('OTP'));
            //await AsyncStorage.setItem('logged', '1');
            //this.props.navigation.navigate('App');


        }
    }
    ok = () => {
        this.setState({ isModalVisible: false })

    }
    fail = () => {
        this.setState({ isModalVisible2: false })
    }

    setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }
    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: "transparent",
                justifyContent: "center"
            }}>
                <StatusBar hidden />
                <ImageBackground style={styles.imgBackground}
                    resizeMode='stretch'
                    source={require('../../assets/Background.png')}>

                    <View style={{
                        flex: 1,
                        backgroundColor: "transparent",
                        justifyContent: "center"
                    }}>
                        <PopUp isModalVisible={this.state.isModalVisible}
                            image={require('../../assets/fail.png')}
                            text="Fail!"
                            text2="Email already exists!"
                            label="Try again"
                            Ok={() => this.ok()} />
                        <PopUp isModalVisible={this.state.isModalVisible2}
                            image={require('../../assets/fail.png')}
                            text="Fail!"
                            text2="Something went wrong. Please try again."
                            label="Try again"
                            Ok={() => this.fail()} />
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}>
                            <Image style={{ marginTop: verticalScale(79), marginLeft: scale(30), width: scale(198), height: scale(24) }}
                                resizeMode='cover'
                                source={require('../../assets/VACCINELEDGER.png')} />
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(8), fontSize: scale(26), color: '#000000', fontFamily: "Roboto-Regular" }}>
                                Welcome
                        </Text>
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(9), fontSize: scale(13), color: '#000000', fontFamily: "Roboto-Light" }}>
                                Signup to continue
                        </Text>

                            <View style={[{ marginLeft: scale(30), marginRight: scale(30), flex:1, borderRadius: 10, backgroundColor: "#FFFFFF", marginTop: verticalScale(40), justifyContent: "center", alignItems: "center" }, shadowStyle]}>
                                <Text style={{ marginTop: verticalScale(20), fontSize: scale(20), color: '#000000', fontFamily: "Roboto-Bold", fontWeight: "bold" }}>
                                    Signup
                        </Text>
                                <View style={{ width: "100%", }}>
                                    <FloatingLabelInput
                                        label="Name"
                                        onChangeText={(name) => this.setState({ name })}
                                        value={this.state.name}
                                        error={this.state.error_name_line}
                                        image={require('../../assets/user.png')}
                                    />
                                    <Text style={{ marginLeft: scale(34), color: "#D80000", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", }}>{this.state.error_name}</Text>
                                </View>

                                <View style={{ width: "100%", marginTop: verticalScale(7) }}>
                                    <FloatingLabelInput
                                        label="Email"
                                        onChangeText={(Email) => this.setState({ Email })}
                                        value={this.state.Email}
                                        image={require('../../assets/mail.png')}
                                        keyboardType={"email-address"}
                                        error={this.state.error_email_line}
                                    />
                                    <Text style={{ marginLeft: scale(34), color: "#D80000", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", }}>{this.state.error_email}</Text>
                                </View>

                                <View style={{ width: "100%", marginTop: verticalScale(7) }}>
                                    <FloatingLabelInput
                                        label="Password"
                                        onChangeText={(pass) => this.setState({ pass })}
                                        value={this.state.pass}
                                        secureTextEntry={this.state.hidePassword}
                                        image={require('../../assets/key.png')}
                                        password={true}
                                        error={this.state.error_pwd_line}
                                        passimage={(this.state.hidePassword) ? require('../../assets/HidePassword.png') : require('../../assets/ShowPassword.png')}
                                        onClick={this.setPasswordVisibility}
                                    />
                                    <Text style={{ marginLeft: scale(34), color: "#D80000", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", }}>{this.state.error_pwd}</Text>
                                </View>

                                {/* <Text style={{ color: "red", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", alignSelf: "center" }}>{this.state.error}</Text> */}
                                <View style={{ height: scale(10) }} />
                                <TouchableOpacity
                                    onPress={this._signup}
                                    style={{ width: scale(133), height: scale(40), borderRadius: 8, backgroundColor: "red", justifyContent: "center", alignItems: "center" }}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                        colors={['#0093E9', '#36C2CF']}
                                        style={{ width: scale(133), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ fontSize: scale(20), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SIGNUP</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", marginTop: verticalScale(17) }}>
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#707070" }}>
                                        Already have an Account?
                </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Auth')}>
                                        <Text style={{ marginLeft: scale(7), fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#0B65C1", fontWeight: "bold" }}>
                                            Log In
                </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: scale(10) }} />
                            </View>
                            <View style={{ height: scale(10) }} />
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
        position: 'absolute',
    },
    wrapper: {
        flex: 1,
    },
})
function mapStateToProps(state) {
    return {
        userdata: state.register.userdata
    }
}

export default connect(mapStateToProps, { saveUser })(Signup)