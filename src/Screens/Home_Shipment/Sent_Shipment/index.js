import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    ActivityIndicator,
    LayoutAnimation, UIManager,
    PermissionsAndroid,
    FlatList,
    Platform,
    RefreshControl,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Shipment_Card from "../../../components/Shipment_Card";
import Empty_Card from "../../../components/Empty_Card";
import { Total_Shipment, Total_Shipment_Sent, Total_Shipment_Received, Current_Shipment_InTransit,customiseShipmentData } from "../../../utils/Shipment_Count";

class Sent_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.props.shipment];
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };
    GetData = async () => {
        await this.props.userinfo()
        await this.props.inventory(0,5)
        await this.props.fetchPublisherShipments(0,5)
        await this.props.fetchAllUsers()
        await this.props.fetch_Purchase_Orders_ids()
        await this.props.fetch_Purchase_Orders()
    }
    onRefresh = async () => {
        //Call the Service to get the latest data
        this.GetData();
      }
    render() {
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        console.log('sennt order', this.props.shipment);
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                //backgroundColor:"#000",

            }}>
                <View style={{ height: 5 }} />
                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                          //refresh control used for the Pull to Refresh
                          onRefresh={this.onRefresh.bind(this)}
                          colors={['#a076e8', '#5dc4dd']}
                        />
                      }>
                    {this.props.shipment.length === 0 ?
                        <View style={{ marginTop: verticalScale(-35) }}>
                            <Empty_Card Text="Shipments are Empty!" />
                        </View>
                        :

                        this.props.shipment.map((item, key) => (
                            <>
                                {
                                    item.status === "Shipped" ?
                                        <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} />
                                        : null
                                }
                            </>
                        ))
                    }

                    <View style={{ height: scale(20) }} />
                </ScrollView>

            </View>
        )
    }
}

// const customiseShipmentData = (shipment) => {

//     let data = [];
//     for (let i in shipment) {
//         const Object = {};
//         const total = 0;
//         if (shipment[i].status === "Shipped") {
//             Object = shipment[i]
//             Object['isExpanded'] = false
//             data.push(Object);
//         }
//     }
//     console.log('data', data);
//     return data.reverse();

// }
function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata,
        logindata: state.login.logindata,
        user: state.userinfo.user,
        loder: state.loder,
        inventorydata: state.inventory.inventorydata,
        shipment: customiseShipmentData(state.shipment.shipmentdata,"Sent_Shipment"),
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments, fetchAllUsers, fetch_Purchase_Orders_ids, fetch_Purchase_Orders })(Sent_Shipment)




