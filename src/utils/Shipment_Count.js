export const Total_Shipment = (shipment) => {
    return shipment.length
}

export const Total_Shipment_Sent = (shipment) => {
    let data = [];
    for (let i in shipment) {
        const Object = {};
        if (shipment[i].status === "Shipped") {
            Object = shipment[i]
            data.push(Object);
        }
    }

    return data.length
}

export const Total_Shipment_Received = (shipment) => {
    let data = [];
    for (let i in shipment) {
        const Object = {};
        if (shipment[i].status === "Received") {
            Object = shipment[i]
            data.push(Object);
        }
    }

    return data.length
}

export const Current_Shipment_InTransit = (shipment) => {
    let data = [];
    for (let i in shipment) {
        const Object = {};
        if (shipment[i].status === "In Transit") {
            Object = shipment[i]
            data.push(Object);
        }
    }

    return data.length
}

export const Count = (data) => {
    var sum = 0;
    if (typeof data == 'object') {
        data.forEach(expire => {
            sum += parseFloat(expire.quantity);
        });
    }
    return sum;
}

export const customiseShipmentData = (shipment, name) => {
    if (name === "HomeShipment") {
        let data = [];
        for (let i in shipment) {
            const Object = {};
            var sum = 0;
            if (typeof shipment == 'object') {
                shipment[i].products.forEach(element => {
                    sum += parseFloat(element.quantity)
                });
            }
            Object = shipment[i]
            Object['isExpanded'] = false
            Object['total'] = sum
            data.push(Object);
        }
        return data.reverse();
    } else if (name === "All_Shipment") {
        let data = [];
        for (let i in shipment) {
            const Object = {};
            var sum = 0;
            if (typeof shipment == 'object') {
                shipment[i].products.forEach(element => {
                    sum += parseFloat(element.quantity)
                });
            }
            Object = shipment[i]
            Object['isExpanded'] = false
            Object['total'] = sum
            data.push(Object);
        }
        return data;
    } else if (name === "Received_Shipment") {
        let data = [];
        for (let i in shipment) {
            const Object = {};
            if (shipment[i].status === "Received") {
                var sum = 0;
                if (typeof shipment == 'object') {
                    shipment[i].products.forEach(element => {
                        sum += parseFloat(element.quantity)
                    });
                }
                Object = shipment[i]
                Object['isExpanded'] = false
                Object['total'] = sum
                data.push(Object);
            }
        }
        return data.reverse();
    } else if (name === "Sent_Shipment") {
        let data = [];
        for (let i in shipment) {
            const Object = {};
            if (shipment[i].status === "Shipped") {
                var sum = 0;
                if (typeof shipment == 'object') {
                    shipment[i].products.forEach(element => {
                        sum += parseFloat(element.quantity)
                    });
                }
                Object = shipment[i]
                Object['isExpanded'] = false
                Object['total'] = sum
                data.push(Object);
            }
        }
        return data.reverse();
    }
}