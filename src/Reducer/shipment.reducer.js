import { SHIPMENT, UPDATE_SHIPMENT, ADD_SHIPMENT, SHIPMENT_COUNTS } from '../constant'
export default function (state = { shipmentdata: [], shipmentcounts: {} }, action) {
    switch (action.type) {
        case SHIPMENT:
            console.log("action.payload",action.payload,[...state.shipmentdata, ...action.payload]);
            return {
                ...state,
                shipmentdata:[...state.shipmentdata, ...action.payload]
                //.map(shipmentdata => JSON.parse(shipmentdata[0].data)),
            }
        case UPDATE_SHIPMENT:
            console.log('update', action.payload);
            console.log('state', state.shipmentdata);
            // console.log('index',this.state.indexOf(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId));
            // const shipmentid = this.state.findIndex(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId)   
            const p = state.shipmentdata.map((item) => {
                if (item.shipmentId === action.payload.shipmentId) {
                    return { ...item, ...action.payload }
                } else {
                    return item
                }
            })
            return {
                ...state,
                shipmentdata: p
            }

        case ADD_SHIPMENT:
            console.log("shipmentdata", action.payload);

            let shipmentdata = state.shipmentdata
            shipmentdata.push(action.payload)
            console.log("data", shipmentdata);
            return {
                ...state,
                shipmentdata: shipmentdata
            }


        case SHIPMENT_COUNTS:
            console.log("action.payloadshipmentcounts",action.payload);
            const totals = [];
            totals.push(action.payload.totalShipments.total)
            totals.push(action.payload.totalShipmentsSent.total)
            totals.push(action.payload.totalShipmentsReceived.total)
            totals.push(action.payload.currentShipments.total)
            console.log("su",totals);

            return {
                ...state,
                shipmentcounts: totals
            }


        default:
            return state
    }
}
