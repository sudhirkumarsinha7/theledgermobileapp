import { INVENTORY, ADD_INVENTORY, INVENTORY_COUNTS, VACCINE_COUNTS } from '../constant'
export default function (state = { inventorydata: [], inventorycounts: {}, vaccinecounts: {} }, action) {
    switch (action.type) {
        case INVENTORY:
            console.log('reducer_inventorydata', action.payload);
            return {
                ...state,
                inventorydata: action.payload
                //action.payload.data.map(inventorydata => JSON.parse(inventorydata.data)),
            }
        case ADD_INVENTORY:
            console.log("action.payload,ivvvv", action.payload);

            return {
                ...state,
                // inventorydata:[...action.payload,...state.inventorydata,]
                inventorydata: [...state.inventorydata, ...action.payload,]
            }

        case INVENTORY_COUNTS:
            return {
                ...state,
                inventorycounts: action.payload
            }

        case VACCINE_COUNTS:
            return {
                ...state,
                vaccinecounts: action.payload
            }
        default:
            return state
    }
}
