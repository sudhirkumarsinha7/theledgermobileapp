## 🚀 Vaccine Ledger

[![React Native](https://img.shields.io/badge/React%20Native-v0.61.5-blue.svg)](https://facebook.github.io/react-native/)
[![React Navigation V2](https://img.shields.io/badge/React%20Navigation-v4.3.1-blue.svg)](https://reactnavigation.org/)

## Features

* [React Navigation](https://reactnavigation.org/)
* [Redux](https://redux.js.org/)
* [Redux Thunk](https://www.npmjs.com/package/redux-thunk)
* [React Redux](https://react-redux.js.org/)

## Prerequisites

* [Node](https://nodejs.org)
* [Yarn](https://yarnpkg.com/)
* A development machine set up for React Native by following [these instructions](https://facebook.github.io/react-native/docs/getting-started.html)

## Getting Started

1. Clone this repo.
2. Go to project's root directory, `cd <your project name>`
3. Run `yarn` or `npm install` to install dependencies
4. Connect a mobile device to your development machine
5. Run the test application:
  * On Android:
    * Metro Bundle `npm start` or `yarn start`
    * Run `npx react-native run-android` or `yarn android`
6. Enjoy!!!

## Clean Gradle

cd android/
chmod +x gradlew
./gradlew clean
cd ..

## Publish App

1. Go to /android/app/build.gradle and change the version code and versionName.
2. cd android/
3. ./gradlew app:assembleRelease
4. Go to /android/app/build/outputs/apk/release and you can see the app-release.apk
5. Then open chrome and go to play console 
6. Project and go to Release Management > App Release > Edit Release
7. Select the app-release.apk file annd upload it.

## if any issue while creating the release apk 

/node_modules/react-native/react.gradle file and add the doLast right after the doFirst block, manually.

doLast {
    def moveFunc = { resSuffix ->
        File originalDir = file("$buildDir/generated/res/react/release/${resSuffix}");
        if (originalDir.exists()) {
            File destDir = file("$buildDir/../src/main/res/${resSuffix}");
            ant.move(file: originalDir, tofile: destDir);
        }
    }
    moveFunc.curry("drawable-ldpi").call()
    moveFunc.curry("drawable-mdpi").call()
    moveFunc.curry("drawable-hdpi").call()
    moveFunc.curry("drawable-xhdpi").call()
    moveFunc.curry("drawable-xxhdpi").call()
    moveFunc.curry("drawable-xxxhdpi").call()
    moveFunc.curry("raw").call()
}

## New guidelines coming soon!
